# Learn SQL In A Weekend

This repository contains the code listings from the appendices of the book, <cite>Learn SQL In A Weekend</cite>, by Kevin Thompson and Deanna Dicken. The companion Web site no longer exists and, it appears, the publisher, Premier Press, went out of business in 2002.

I could not find the downloadable source code anywhere on Internet, but I found a PDF of the book and copied-and-pasted the source code into plain text files. I corrected a few minor errors, and may have missed a few as well.

You can [buy a copy of the book from Amazon](https://www.amazon.com/Learn-SQL-Weekend-Deanna-Dicken/dp/1931841624). (This is _not_ an affiliate link.)

This file is intended as a service for those who wish to use the book as I did, and I believe posting these code listings complies with the Fair Use provision of copyright law. That said, if you are the copyright holder and wish this to be taken down, I will do so at your request, and I respectfully ask that you provide the code listings yourself. They may be linked at [the book's page at the Open Library Project](https://openlibrary.org/works/OL8954296W/Learn_SQL_in_a_weekend).
